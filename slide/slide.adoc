= Anbox補足とCryptomatorでクラウドファイルを暗号化
Kenichiro Matohara(matoken) <maroken@kagolug.org>
:revnumber: 1.0
:revdate: 2021-09-25(Sat)
:revremark: 鹿児島らぐ 2021.09「{doctitle}」
:homepage: https://matoken.org/
:imagesdir: resources
:data-uri:
:example-caption: 例
:table-caption: 表
:figure-caption: 図
:backend: revealjs
:revealjs_theme: serif
:customcss: resources/my-css.css
:revealjs_slideNumber: c/t
:title-slide-transition: none
:icons: font
:revealjs_hash: true
:revealjs_center: true
:revealjs_autoPlayMedia: true
:revealjs_transition: false  
:revealjs_transitionSpeed: fast

== link:https://matoken.org[Kenichiro Matohara(matoken) https://matoken.org]

image::map.jpg[background, size=cover]

* 南隅から参加(鹿児島の右下)
* 好きなLinuxディストリビューションはDebian
* お仕事募集 mailto:work＠matohara.org

== 最近

* 古いスマホが壊れたぽい(2013年製のAndroid 5端末)
* 楽天モバイル無料期間が今月まで -> 今月下旬リリースの povo2.0 を契約?(1日使い放題トッピングが220円->330円になるけど基本料0円に)

== Anbox(Android in a Box)

* AndroidをLinuxで動かすもの
 link:https://kagolug.connpass.com/event/174375/[鹿児島Linux勉強会 2020.04] にて発表
** link:https://speakerdeck.com/matoken/linuxshang-deanboxwoli-yong-siteandroidapuriwoshi-xing[Linux上でAnBoxを利用してAndroidアプリを実行]
* Kernel moduleが必要，しかしLinux 5.7 以降使えなくなった
* 最近のKernelであれば取り込まれているで無効なら有効にすればOK

----
$ grep ANDROID /boot/config-$(uname -r)
CONFIG_ANDROID=y
CONFIG_ANDROID_BINDER_IPC=y
CONFIG_ANDROID_BINDERFS=y
CONFIG_ANDROID_BINDER_DEVICES="binder"
CONFIG_ANDROID_BINDER_IPC_SELFTEST=y
----

=== !

----
$ sudo git -C /etc diff HEAD~ /etc/default/grub
diff --git a/default/grub b/default/grub
index 926dd2b..a67db35 100644
--- a/default/grub
+++ b/default/grub
@@ -6,7 +6,7 @@
 GRUB_DEFAULT=0
 GRUB_TIMEOUT=5
 GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
-GRUB_CMDLINE_LINUX_DEFAULT=""
+GRUB_CMDLINE_LINUX_DEFAULT="binder.devices=binder,hwbinder,vndbinder,anbox-binder,anbox-hwbinder,anbox-vndbinder"
 #GRUB_CMDLINE_LINUX_DEFAULT="intel_palate-no_hwp i915.preliminary_hw_support=1"
 GRUB_CMDLINE_LINUX=""
 
$ sudo update-grub
$ sudo shutdown -r now
----

=== Waydroid というものも見つけた

* Linux(Wayland)上でAndroid

まだうまく動かせていない……

https://waydro.id/

== クラウドのファイルを暗号化しよう

* link:https://support.google.com/drive/thread/127021326/google-has-locked-my-account-for-sharing-a-historical-archive-they-labeled-as-terrorist-activity?hl=en[Google has locked my account for sharing a historical archive they labeled as "terrorist Activity" - Google Drive Community]
* ミリタミー趣味とかはあまりないけどもセキュリティ関連の文章とかを置いてても同じようにテロ活動としてBANされかねないような……
* E2EEなサービス?
* 自分で建てる(ちょっと面倒)
* 暗号化してクラウドに保存しておけば大丈夫なはず

== 過去の試み

* link:https://speakerdeck.com/matoken/slax-dean-hao-hua-fs-wochi-tiyun-bu[Slax で暗号化fs を持ち運ぶ - Speaker Deck]
* link:https://www.slideshare.net/matoken/notepc-hdd-1102[偏執的な人の為の? NotePCセキュリティ HDD編 -小江戸らぐ1月のオフな集まり(第102回)-]
* link:https://www.slideshare.net/matoken/lessfs-110[Lessfs をかじってみた(小江戸らぐオフな集まり第110回)]
* link:https://speakerdeck.com/matoken/ext4falsehuairubesuan-hao-hua-fswoshi-sitemiru[ext4のファイルベース暗号化FSを試してみる - Speaker Deck]
* link:https://speakerdeck.com/matoken/veracryptwozulucryptde[VeraCryptをzuluCryptで - Speaker Deck]
* link:https://matoken.org/blog/2015/09/12/try-reverse-mode-of-encryption-file-system-encfs/[暗号化ファイルシステム EncFS の Reverse mode を試す]
* link:https://matoken.org/blog/2015/09/17/try-encfs4winy-of-encryption-fs-encfs-windows-port/[暗号化FS EncFS Windows 移植版の EncFS4Winy を試す]
* link:https://matoken.org/blog/2016/02/07/try-cryfs-of-the-encrypted-file-system-similar-to-encfs/[EncFSに似た暗号化ファイルシステムのCryFSを試す]

== 暗号化ファイルシステム

暗号化ファイルをクラウド上に置き，同期したコンピュータ上で透過的に復号化して利用．

----
(( 🔐🗃️ )) <-🔐-> 🖥️🔑📁
            <-🔐-> 💻️🔑📁
            <-🔐-> 📱🔑📁
----

* VeraCrypt/LUKS(dm-crypt)
ファイル群が1ボリュームに格納され，1ファイルの更新でも全領域の同期がされるのでクラウド向けではない
* encfs/CryFS/lessfs/eCryptfs/Cryptomator……
1ファイルが1暗号化ファイル(くらい)に対応しているので更新ファイルだけの同期で済むのでクラウドで同期しやすい

今回新たに *Cryptomator* を試した


////
=== encfs/eCryptfs/Cryptomator……

* encfs
** 利用はお手軽基本cui
** GUI利用．CryptKeeper/zulucrypt-gui/zulumount-gui
* eCryptFS
** ログイン時に透過的にマウントするには便利
** 関連ファイルが複数あるので少し面倒
** GUI利用．zulucrypt-gui/zulumount-gui
* Cryptomator
** OSS,第三者機関での監査，マルチプラットホーム(iOS/Androidを含む)
** GUI/CLI利用可能

////

== Cryptomator

link:https://cryptomator.org/[Cryptomator - Free Cloud Encryption for Dropbox & Co]

* クラウドサービス向けの暗号化ファイルシステムのよう
* OSS,第三者機関での監査，マルチプラットホーム(iOS/Androidを含む)
* GUI/CLI標準で利用可能

=== 入手と実行

----
$ wget https://github.com/cryptomator/cryptomator/releases/download/1.5.15/cryptomator-1.5.15-x86_64.AppImage \
https://github.com/cryptomator/cryptomator/releases/download/1.5.15/cryptomator-1.5.15-x86_64.AppImage.asc
$ cat << __EOF__ > ./key                                             
-----BEGIN PGP PUBLIC KEY BLOCK-----
  :
-----END PGP PUBLIC KEY BLOCK-----
__EOF__
$ gpg --verify ./cryptomator-1.5.15-x86_64.AppImage.asc 
gpg: assuming signed data in './cryptomator-1.5.15-x86_64.AppImage'
gpg: Signature made Wed Apr 21 19:48:10 2021 JST
gpg:                using RSA key 58117AFA1F85B3EEC154677D615D449FE6E6A235
gpg: Good signature from "Cryptobot <releases@cryptomator.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 5811 7AFA 1F85 B3EE C154  677D 615D 449F E6E6 A235
$ chmod u+x ./cryptomator-1.5.15-x86_64.AppImage
$ ./cryptomator-1.5.15-x86_64.AppImage
----

=== GUIで金庫を作る

image:20210925_07:09:03-28424.jpg[]

=== !

image:20210925_07:09:12-28588.jpg[]

=== !

image:20210925_07:09:24-28814.jpg[]

=== !

image:20210925_07:09:57-29441.jpg[]

=== !

image:20210925_07:09:48-31427.jpg[]

=== !

image:20210925_07:09:30-32274.jpg[]

=== !

image:20210925_07:09:43-32499.jpg[]

=== !

image:20210925_07:09:14-33073.jpg[]

=== !

image:20210925_07:09:36-33646.jpg[]

== マウントオプション

image:20210925_07:09:37-60290.jpg[]

=== !

image:20210925_07:09:42-60397.jpg[]

=== !

image:20210925_07:09:51-60571.jpg[]

=== !

image:20210925_07:09:56-60648.jpg[]

== 設定

image:20210925_13:09:51-416864.jpg[]

=== !

image:20210925_13:09:05-417146.jpg[]

FUSE or WebDAV

=== !

image:20210925_13:09:11-417315.jpg[]

=== !

image:20210925_13:09:16-417400.jpg[]

=== !

image:20210925_13:09:20-417495.jpg[] 

=== ファイルの場所

暗号化ファイル::
`~/Yandex.disk/大切な金庫`
複合ファイル::
`~/.local/share/Cryptomator/mnt/大切な金庫`

=== ファイルを作成して暗号化状態を確認

----
$ tree ~/Yandex.Disk/大切な金庫/ <.>
/home/matoken/Yandex.Disk/大切な金庫/
├── IMPORTANT.rtf
├── d
│   └── Q2
│       └── OKK5TINV6Q3GFWETLKX6RSIUF3XETG
│           └── 5qb56BO3tx4bSZKY-r-XTvTqp7dVbTBdO0j5.c9r
├── masterkey.cryptomator
└── masterkey.cryptomator.F0FD16CE.bkup

3 directories, 4 files
$ echo 'HELLO' > /home/matoken/.local/share/Cryptomator/mnt/大切な金庫/hello.txt <.>
----

<.> 初期状態のファイル，ディレクトリ
<.> ファイルを作ってみる

=== !

----
$ tree ~/Yandex.Disk/大切な金庫/ <.>
/home/matoken/Yandex.Disk/大切な金庫/
├── IMPORTANT.rtf
├── d
│   └── Q2
│       └── OKK5TINV6Q3GFWETLKX6RSIUF3XETG
│           ├── 5qb56BO3tx4bSZKY-r-XTvTqp7dVbTBdO0j5.c9r
│           └── l8hk2h_zzd0fr4fhnxSxB22dQ004nHuXaw==.c9r <.>
├── masterkey.cryptomator
└── masterkey.cryptomator.F0FD16CE.bkup

3 directories, 5 files
$ od -c ~/Yandex.Disk/大切な金庫/d/Q2/OKK5TINV6Q3GFWETLKX6RSIUF3XETG/l8hk2h_zzd0fr4fhnxSxB22dQ004nHuXaw\=\=.c9r <.>
0000000 314 221   \ 206 335 370   L 337 263 213 322   r 243 365   E 352
0000020  \a 246 272 022   V  \n 276   R 244 326 360   o   U 246 346  \a
0000040 207 261   : 265 023 033   3   0 331 330   5 215 217 353 201 315
0000060 300   A 340 037   Y   h   K   1 300 241   '   9   e   L   7   !
0000100   8   X   n 267   E   V   O 274   ] 025 271   7   b 255   3   ]
0000120   ? 207 370   V 304 244 333   '   L 335 001   <   N 263 356   y
0000140   t   h   K   [ 356 361 370 264 346   7 326 247 214 213 257 344
0000160 224 004   D 237 335 311 216 320   1 317   d   Y   x 357   8   :
0000200   U   (   0   { 315 202 354 031   y   ? 204   O 321 342
0000216
----

<.> 再度ファイル，ディレクトリを確認
<.> ファイルが増えた
<.> ファイル名も内容も暗号化されている

=== CUI

----
$ java -jar ./cryptomator-cli-0.4.0.jar
14:01:37.499 [main] ERROR org.cryptomator.cli.CryptomatorCli - No vault specified.
usage: java -jar cryptomator-cli.jar --bind localhost --port 8080 --vault
            mySecretVault=/path/to/vault --password
            mySecretVault=FooBar3000 --vault
            myOtherVault=/path/to/other/vault --password
            myOtherVault=BarFoo4000 --vault
            myThirdVault=/path/to/third/vault --passwordfile
            myThirdVault=/path/to/passwordfile
    --bind <WebDAV bind address>                TCP socket bind address of
                                                the WebDAV server. Use
                                                0.0.0.0 to accept all
                                                incoming connections.
    --password <Password of a vault>            Format must be
                                                vaultName=password
    --passwordfile <Passwordfile for a vault>   Format must be
                                                vaultName=passwordfile
    --port <WebDAV port>                        TCP port, the WebDAV
                                                server should listen on.
    --vault <Path of a vault>                   Format must be
                                                vaultName=/path/to/vault
----

== ファイル名長

----
$ getconf NAME_MAX .
254
$ num=1;str="a";while :; do if ! touch $str; then echo $num; break; fi; $((num++)); str="${str}a"; done
touch: cannot touch 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa': File name too long
1025
----

通常は255

== mobile APPちょっと高め?

image:iOS.PNG[]
image:Android.jpg[]

F-Droid版は無料かと思ったけどライセンス鍵を求められた

== 類似ツール

* link:https://github.com/rfjakob/gocryptfs[rfjakob/gocryptfs: Encrypted overlay filesystem written in Go]

== まとめ

* EncFSと同じように使いやすいうえにマルチプラットホーム(EncFSにもEncFS4Winyなどが)
* 公式のGUIがあって日本語ローカライズもされていておすすめしやすい
* クラウドドライブ向け
* いろいろなネットワークドライブを暗号化しよう :)

== 余録: オンラインストレージ招待リンク

* link:https://db.tt/2KWngmxIfQ[Dropbox 2GB + 500MB]
* link:https://onedrive.live.com/?invref=000f8195e941d76a&invscr=90[One Drive 5GB +500MB]
* link:https://disk.yandex.com/invite/?hash=URJFTFG9[Yandex Disk 10GB + 500MB]
* link:https://my.pcloud.com/#page=register&invite=mVfNZixewN7[pCloud 10GB + 1GB]
* link:https://www.pcloud.com/welcome-to-pcloud/?discountcode=jNskgG2lgM5C6kz7DTBD6BZV[pCloud Premium account ($5 value) for free for 1 month.]
* link:https://cloud.degoo.com/drive-3wnve0ug9i7c[Degoo 100GB + 5GB]

== 奥付

* 発表
** link:https://kagolug.connpass.com/event/223477/[鹿児島Linux勉強会 2021.09(オンライン開催)]
* 発表者
** link:https://matoken.org/[Kenichiro Matohara(matoken)]
* 利用ソフトウェア
** link:https://github.com/asciidoctor/asciidoctor-reveal.js[Asciidoctor Reveal.js]
* ライセンス
** CC BY-NC-SA 4.0
